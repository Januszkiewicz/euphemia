module.exports = {
	purge: {
		enabled: true,
		content: ['./assets/js/januszkiewicz.js', './Controllers/**/*.phtml']
	},
	darkMode: false,
	variants: {
		width: ['responsive'],
	},
	theme: {
		fontFamily: {
			sans:["Roboto", "sans-serif"]
		},
		fontSize: {
			's': ['.875rem', '1rem'],
			'm': '1rem',
			'l': '1.375rem',
			'xl': '1.875rem'
		},
		colors: {
			current: "currentColor",
			transparent: 'transparent',
			black: '#3d3d3d',
			blue: '#0747a6',
			white: {
				100: '#ffffff',
				200: '#fbfbfb',
				300: '#ececec',
			},
			gray: '#9f9f9f'
		},
		screens: {
			xsMin: {min: '480px'},
			sMin: {min: '800px'},
			sMax: {min: '799px'},
			mMin: {min: '1024px'},
			lMin: {min: '1280px'},
			lMax: {max: '1279px'},
			xlMin: {min: '1440px'},
			xlMax: {max: '1439px'},
		},
		maxWidth: {
			xxxs: '480px',
			xxs: '640px',
			xs: '800px',
			s: '1024px',
			m: '1280px',
			l: '1440px'
		},
		extend: {
			spacing: {
				xs: '0.5rem',
				s: '0.75rem',
				m: '1rem',
				l: '1.5rem',
				xl: '2rem',
				xxl: '2.5rem'
			},
			width: {
				xs: '128px',
				s: '180px',
				m: '200px',
				xl: '360px'
			},
			height: {
				xs: '128px',
				s: '180px',
				m: '200px',
				xl: '360px'
			}
		}
	},
	plugins: [],
}
