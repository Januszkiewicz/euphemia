A = function(pv_target, pv_method, pv_params) {
	if(typeof pv_target === "string") {
		var elements = document.querySelectorAll(pv_target);

		if(pv_method !== undefined) {
			for(var i = elements.length - 1; i > -1; --i) {
				pv_method.call(elements[i], pv_params);
			}
		}

		return 1 === elements.length ? elements[0] : elements;
	}
	else if(pv_method !== undefined) {
		if(pv_target instanceof HTMLElement) {
			pv_method.call(pv_target, pv_params);
		}
		else if((pv_target instanceof HTMLCollection || pv_target instanceof NodeList)) {
			for(var i = pv_target.length - 1; i > -1; --i) {
				pv_method.call(pv_target[i], pv_params);
			}
		}

		return pv_target;
	}
}

A.CreateElement = function(pv_html) {
	var element = document.createElement("div");

	element.innerHTML = pv_html;

	return element.firstElementChild;
}

A.RemoveElement = function(pv_element) {
	pv_element.parentElement.removeChild(pv_element);
}

A.AddClass = function(pv_params) {
	if(pv_params.disableTransitions === true) {
		var transition = this.style.transition;

		this.style.transition = "none";

		this.classList.add(pv_params.name);

		this.scrollHeight;

		this.style.transition = transition;
	}
	else {
		this.classList.add(pv_params.name);
	}
}

A.RemoveClass = function(pv_params) {
	if(pv_params.disableTransitions === true) {
		var transition = this.style.transition;

		this.style.transition = "none";

		this.classList.remove(pv_params.name);

		this.scrollHeight;

		this.style.transition = transition;
	}
	else {
		this.classList.remove(pv_params.name);
	}
}

A.ToggleClass = function(pv_params) {
	if(pv_params.disableTransitions === true) {
		var transition = this.style.transition;

		this.style.transition = "none";

		this.classList.toggle(pv_params.name);

		this.scrollHeight;

		this.style.transition = transition;
	}
	else {
		this.classList.toggle(pv_params.name);
	}
}

A.ReplaceClass = function(pv_params) {
	A(this, A.RemoveClass, {name: pv_params.from});

	A(this, A.AddClass, {name: pv_params.to});
}

A.GetOffsetHeight = function() {
    var transition = this.style.transition;

    var height = this.style.height;

    this.style.transition = "none";

    this.style.height = "auto";

    var offsetHeight = this.offsetHeight;

    this.style.transition = transition;

    this.style.height = height;

    return offsetHeight;
}

A.AjaxSlideOver = {
	scroll_bar_width: 17
}

A.AjaxSlideOver.Load = function(pv_params) {
	var request = new XMLHttpRequest("GET");

	request.open("GET", pv_params.url, true);

	request.onreadystatechange = function() {
		if(request.readyState === 4) {
			if(request.status === 200) {
				A.AjaxSlideOver.OnLoadSuccess.call(this, request);
			}
			else {
				A.AjaxSlideOver.OnLoadError.call(this, request);
			}
		}
	};

	request.send();
}

A.AjaxSlideOver.OnLoadSuccess = function(pv_request) {
	document.body.style.overflow = "hidden";

	document.body.style.paddingRight = A.AjaxSlideOver.scroll_bar_width + "px";

	var object = {};

	object.content_element = A.CreateElement(pv_request.responseText);

	object.overlay_element = A.CreateElement("<div class=\"fixed inset-0 transition-opacity opacity-0 overflow-hidden\" onclick=\"A(this, A.AjaxSlideOver.Close)\"><div class=\"absolute inset-0 bg-gray opacity-75\"></div></div>");

	object.overlay_element.style.marginRight = A.AjaxSlideOver.marginRight = 17 + "px";

	var window_element = A.CreateElement("<div class=\"fixed inset-0 w-full h-full overflow-y-auto overflow-x-hidden\"></div>");

	window_element.pv_ajax_window = object;

	window_element.appendChild(object.content_element);

	window_element.appendChild(object.overlay_element);

	document.body.appendChild(window_element);

	window_element.scrollHeight;

	A(window_element.pv_ajax_window.overlay_element, A.ReplaceClass, {from:"opacity-0", to:"opacity-100"});

	A(window_element.pv_ajax_window.content_element, A.ReplaceClass, {from:"opacity-0", to:"opacity-100"});

	A(window_element.pv_ajax_window.content_element, A.ReplaceClass, {from: "translate-x-full", to:"translate-x-0"});

	A.Initialize.call(window_element);
}

A.AjaxSlideOver.OnLoadError = function() {
	alert("Now why'd you do that?");
}

A.AjaxSlideOver.Close = function(pv_params) {
	var element = this;

	while(element.pv_ajax_window === undefined && element.parentElement !== null) {
		element = element.parentElement;
	}

	if(element.pv_ajax_window !== undefined) {
		A(element.pv_ajax_window.overlay_element, A.ReplaceClass, {from: "opacity-100", to:"opacity-0"});

		A(element.pv_ajax_window.content_element, A.ReplaceClass, {from: "opacity-100", to:"opacity-0"});

		A(element.pv_ajax_window.content_element, A.ReplaceClass, {from: "translate-x-0", to:"translate-x-full"});

		element.pv_ajax_window.content_element.pv_ajax_window_owner = element;

		element.pv_ajax_window.content_element.addEventListener("transitionend", A.AjaxSlideOver.OnCloseTransitionEnd);
	}
}

A.AjaxSlideOver.OnCloseTransitionEnd = function() {
	this.removeEventListener("transitionend", A.AjaxSlideOver.OnCloseTransitionEnd);

	document.body.style.overflow = "";

	document.body.style.paddingRight = "";

	A.RemoveElement(this.pv_ajax_window_owner);
}

A.Gestures = {};

A.Gestures.Swipe = {};

A.Gestures.Swipe.Initialize = function() {
	var object = {};

	object.direction = "ltr";

	object.start_position = 0;

	object.end_position = 0;

	object.on_success = this.hasAttribute("a:gesture-swipe-on-success") ? new Function(this.getAttribute("a:gesture-swipe-on-success")) : null;

	object.swipe_distance_threshold = this.offsetWidth / 4;

	this.pv_gesture_swipe = object;

	this.addEventListener("touchstart", A.Gestures.Swipe.OnTouchStart);
}

A.Gestures.Swipe.OnTouchStart = function(event) {
	this.pv_gesture_swipe.start_position = Math.round(event.touches[0].screenX);

	object = this;

	this.pv_gesture_swipe.on_move = function(event) {
		A.Gestures.Swipe.OnTouchMove.call(object, event);
	}

	window.addEventListener("touchmove", this.pv_gesture_swipe.on_move);

	this.addEventListener("touchend", A.Gestures.Swipe.OnTouchEnd);
}

A.Gestures.Swipe.OnTouchMove = function(event) {
	this.style.transitionDuration = "0ms";

	this.pv_gesture_swipe.current_position = Math.round(event.changedTouches[0].screenX - this.pv_gesture_swipe.start_position);

	if(this.pv_gesture_swipe.current_position > 0) {
		this.style.transform = "translateX(" + this.pv_gesture_swipe.current_position + "px)";
	}
}

A.Gestures.Swipe.OnTouchEnd = function(event) {
	this.style.transitionDuration = "";

	this.style.transform = "";

	this.pv_gesture_swipe.end_position = Math.round(event.changedTouches[0].screenX);

	window.removeEventListener("touchmove", this.pv_gesture_swipe.on_move);

	this.removeEventListener("touchend", A.Gestures.Swipe.OnTouchEnd);

	var swipe_distance = this.pv_gesture_swipe.end_position - this.pv_gesture_swipe.start_position;

	var is_correct_direction = false;

	if(this.pv_gesture_swipe.direction === "ltr") {
		is_correct_direction = this.pv_gesture_swipe.end_position > this.pv_gesture_swipe.start_position;
	}

	if(is_correct_direction && swipe_distance > this.pv_gesture_swipe.swipe_distance_threshold) {
		A(this, A.AjaxSlideOver.Close);
	}
}

A.Animations = {};

A.Animations.Slide = {};

A.Animations.Slide.Up = function() {
	this.addEventListener("transitionend", A.Animations.Slide.TransitionEnd);

	this.style.height = A.GetOffsetHeight.call(this) + "px";

	this.scrollHeight;

	A(this, A.ReplaceClass, {from: "down", to: "up"});
}

A.Animations.Slide.Down = function() {
	A(this, A.AddClass, {name: "up"});

	A(this, A.RemoveClass, {name: "up", disableTransitions: true});

	var height = A.GetOffsetHeight.call(this);

	A(this, A.AddClass, {name: "up", disableTransitions: true});

	this.style.height = height + "px";

	this.style.overflow = "hidden";

	this.addEventListener("transitionend", A.Animations.Slide.TransitionEnd);

	this.scrollHeight;

	A(this, A.ReplaceClass, {from: "up", to:"down"});
}

A.Animations.Slide.Toggle = function() {
	if(this.className.indexOf("down") !== -1) {
		A.Animations.Slide.Up.call(this);
	}
	else {
		A.Animations.Slide.Down.call(this);
	}
}

A.Animations.Slide.TransitionEnd = function() {
	this.removeEventListener("transitionend", A.Animations.Slide.TransitionEnd);

	this.style.height = "";

	this.style.overflow = "";
}

A.Initializators = [];

A.Initializators.push({
	selector: "*[a\\:gesture-swipe]",
	method: A.Gestures.Swipe.Initialize
});

A.Initialize = function() {
	if(this instanceof HTMLElement) {
		for(var i = A.Initializators.length - 1; i > -1; --i) {
			var elements = this.querySelectorAll(A.Initializators[i].selector);

			for(var i = elements.length - 1; i > -1; --i) {
				A.Initializators[i].method.call(elements[i]);
			}
		}
	}
	else {
		for(var i = A.Initializators.length - 1; i > -1; --i) {
			A(A.Initializators[i].selector, A.Initializators[i].method);
		}
	}
}

window.addEventListener("load", A.Initialize);

A.SelectLanguage = function(pv_params) {
	A.ToggleLanguageHighlighting(pv_params.pv_value);

	A.ToggleLanguageDescriptionVisibility(pv_params.pv_value);

	A.ToggleProjectVisibility(this.className.split(" ").indexOf("selected") !== -1 ? pv_params.pv_value : null);
}

A.ToggleLanguageHighlighting = function(pv_value) {
	var elements = A("*[data-language]");

	for(var i = elements.length - 1; i > -1; --i) {
		if(elements[i].dataset.language.indexOf(pv_value) === -1) {
			A(elements[i], A.RemoveClass, {name:"selected"});
		}
		else {
			A(elements[i], A.ToggleClass, {name:"selected"});
		}
	}
}

A.ToggleLanguageDescriptionVisibility = function(pv_value) {
	var elements = A("*[data-language-section-visibility-by-language]");

	for(var i = elements.length - 1; i > -1; --i) {
		if(elements[i].dataset.languageSectionVisibilityByLanguage.indexOf(pv_value) === -1) {
			A(elements[i], A.AddClass, {name:"hidden"});
		}
		else {
			A(elements[i], A.ToggleClass, {name:"hidden"});
		}
	}
}

A.ToggleProjectVisibility = function(pv_value) {
	var elements = A("*[data-project-visibility-by-language]");

	if(pv_value !== null) {
		for(var i = elements.length - 1; i > -1; --i) {
			if(elements[i].dataset.projectVisibilityByLanguage.split(",").indexOf(pv_value) === -1) {
				A(elements[i], A.AddClass, {name:"hidden"});
			}
			else {
				A(elements[i], A.RemoveClass, {name: "hidden"});
			}
		}
	}
	else {
		A(elements, A.RemoveClass, {name:"hidden"});
	}
}