<?
require '../photine/Framework/Framework.php';

\Framework\Run([
	'Config/Languages.json',
	'Config/Projects.json',
	'Config/Routes.json',
	'Config/Actions.json',
	'Config/Controllers.json'
]);
?>