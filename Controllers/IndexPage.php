<?
class IndexPage extends \Framework\Controller {
	/* Instance life cycle methods */

	public function Initialize() {
		$this->Model->Languages = \Framework\GetConfig('Languages');

		$this->Model->Projects = \Framework\GetConfig('Projects');

		$this->Model->Hash = parse_url($_SERVER['REQUEST_URI']);

		$this->Initialize»Controls();

		parent::Initialize();
	}

	private function Initialize»Controls() {
		foreach($this->Model->Projects as $i => $project) {
			$this->CloneControl(
				'Project',
				'Project.'.$i,
				[
					'I' => $i,
					'Project' => $project,
					'Hash' => $this->Model->Hash
				]
			);
		}
	}
}
?>