<?
namespace IndexPage;

class ProjectControl extends \Framework\Controller {
	/* Instance life cycle methods */

	public function Initialize() {
		$this->Model->Url = '/project/'. $this->Model->I;

		$this->Model->LeadParagraph = $this->Initialize»LeadParagraph($this->Model->Project['Description']);

		parent::Initialize();
	}

	private function Initialize»LeadParagraph(string $text): string {
		$delimiter = '%["Cut"]%';

		$lead_paragraph = wordwrap($text, 120, $delimiter, false);

		$occurance = strpos($lead_paragraph, $delimiter);

		$lead_paragraph = substr($lead_paragraph, 0, $occurance !== false ? $occurance : null);

		if($occurance !== false) {
			$lead_paragraph .= '...';
		}

		return $lead_paragraph;
	}
}
?>