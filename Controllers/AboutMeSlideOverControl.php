<?
class AboutMeSlideOverControl extends \Framework\Controller {
	/* Instance life cycle methods */

	public function Initialize() {
		$this->Model->Age = date_create_from_format('Y-m-d', '1997-3-18')->setTime(0, 0, 0)->diff((new DateTime()))->y;

		parent::Initialize();
	}
}
?>